package com.kenfogel.isp.data;

import javafx.beans.property.StringProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 */
public class ISPFXBean {

    private final StringProperty planType;
    private final DoubleProperty totalHours;
    private final DoubleProperty planCost;
    private final DoubleProperty hourlyCost;
    private final DoubleProperty includedHours;

    public ISPFXBean() {
        this.planType = new SimpleStringProperty("A");
        this.totalHours = new SimpleDoubleProperty(0.0);
        this.planCost = new SimpleDoubleProperty(0.0);
        this.hourlyCost = new SimpleDoubleProperty(0.0);
        this.includedHours = new SimpleDoubleProperty(0.0);
    }

    public ISPFXBean(String planType,  double planCost, double totalHours) {
        this.planType = new SimpleStringProperty(planType);
        this.totalHours = new SimpleDoubleProperty(totalHours);
        this.planCost = new SimpleDoubleProperty(planCost);
        this.hourlyCost = new SimpleDoubleProperty(0.0);
        this.includedHours = new SimpleDoubleProperty(0.0);
    }

    public void setPlanType(String planType) {
        this.planType.set(planType);
    }

    public String getPlanType() {
        return planType.get();
    }

    public StringProperty planTypeProperty() {
        return planType;
    }

    public void setPlanCost(double planCost) {
        this.planCost.set(planCost);
    }

    public double getPlanCost() {
        return planCost.doubleValue();
    }

    public DoubleProperty planCostProperty() {
        return planCost;
    }

    public void setTotalHours(double totalHours) {
        this.totalHours.set(totalHours);
    }

    public double getTotalHours() {
        return totalHours.get();
    }

    public DoubleProperty totalHoursProperty() {
        return totalHours;
    }

    public void setHourlyCost(double hourlyCost) {
        this.hourlyCost.set(hourlyCost);
    }

    public double getHourlyCost() {
        return hourlyCost.doubleValue();
    }

    public DoubleProperty hourlyCostProperty() {
        return hourlyCost;
    }

    public void setIncludedHours(double includedHours) {
        this.includedHours.set(includedHours);
    }

    public double getIncludedHours() {
        return includedHours.get();
    }

    public DoubleProperty includedHoursProperty() {
        return includedHours;
    }
}
