package com.kenfogel.isp.data;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class contains the values that are calculated.
 *
 * Add clearBean method
 *
 * @author Ken
 * @version 1.1
 */
public class ISPFXCalculatedBean {

    private final static Logger LOG = LoggerFactory.getLogger(ISPFXCalculatedBean.class);

    // Computed fields on form
    private final StringProperty planType;
    private final DoubleProperty extraHours;
    private final DoubleProperty extraCost;
    private final DoubleProperty totalCost;

    public ISPFXCalculatedBean() {
        this.planType = new SimpleStringProperty("A");
        this.extraHours = new SimpleDoubleProperty(0.0);
        this.extraCost = new SimpleDoubleProperty(0.0);
        this.totalCost = new SimpleDoubleProperty(0.0);
    }

    public void setPlanType(String planType) {
        this.planType.set(planType);
    }

    public String getPlanType() {
        return planType.get();
    }

    public StringProperty planTypeProperty() {
        return planType;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost.set(totalCost);
    }

    public double getTotalCost() {
        return totalCost.doubleValue();
    }

    public DoubleProperty totalCostProperty() {
        return totalCost;
    }

    public void setExtraHours(double extraHours) {
        this.extraHours.set(extraHours);
    }

    public double getExtraHours() {
        return extraHours.get();
    }

    public DoubleProperty extraHoursProperty() {
        return extraHours;
    }

    public void setExtraCost(double extraCost) {
        this.extraCost.set(extraCost);
    }

    public double getExtraCost() {
        return extraCost.get();
    }

    public DoubleProperty extraCostProperty() {
        return extraCost;
    }

    public void clearBean() {
        this.setTotalCost(0.0);
        this.setExtraCost(0.0);
        this.setExtraHours(0.0);
    }

}
