package com.kenfogel.isp.presentation;

import com.kenfogel.controllers.ISPFormController;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * How to launch an application that uses an fxml file
 *
 * @author Ken
 */
public class ISPFXApp extends Application {

    private final static Logger LOG = LoggerFactory.getLogger(ISPFXApp.class);

    private Stage primaryStage;
    private GridPane theISPLayout;

    /**
     * Default constructor
     *
     */
    public ISPFXApp() {
        super();
    }

    /**
     * Method from Application that you must override
     *
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("ISP Applications");

        // Set the application icon using getResourceAsStream.
        // You need a jpeg or jpg file of an image no larger than 32 x 32
        this.primaryStage.getIcons().add(
                new Image(ISPFXApp.class
                        .getResourceAsStream("/images/WindsorKenIcon.jpg")));

        initRootLayout();
        primaryStage.show();
    }

    /**
     * Load the layout and controller.
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ISPFXApp.class
                    .getResource("/fxml/ISPForm.fxml"));
            theISPLayout = (GridPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(theISPLayout);
            primaryStage.setScene(scene);

            // If you need to send messages to the controller you can retrieve the refernce to it.
            ISPFormController controller = loader.getController();

        } catch (IOException e) {
            LOG.error("Error displaying layout", e);
        }
    }

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        // launch is a static method that begins the JavaFX program
        launch(args);
        System.exit(0);
    }
}
