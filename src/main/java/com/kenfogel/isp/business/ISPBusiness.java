package com.kenfogel.isp.business;

import javafx.collections.ObservableList;

/**
 * Interface for the business class
 * 
 * @author kfogel
 */
public interface ISPBusiness {
    
    public void calculate();
    
    public void updateISPFXBean();
    
    public ObservableList<String> getPlans();
}
