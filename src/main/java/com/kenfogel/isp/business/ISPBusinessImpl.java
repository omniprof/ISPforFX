package com.kenfogel.isp.business;

import static com.kenfogel.isp.constants.ISPConstants.ADDCOST_A;
import static com.kenfogel.isp.constants.ISPConstants.ADDCOST_B;
import static com.kenfogel.isp.constants.ISPConstants.ADDCOST_C;
import static com.kenfogel.isp.constants.ISPConstants.INCLUDEDHOURS_A;
import static com.kenfogel.isp.constants.ISPConstants.INCLUDEDHOURS_B;
import static com.kenfogel.isp.constants.ISPConstants.INCLUDEDHOURS_C;
import static com.kenfogel.isp.constants.ISPConstants.PLANRATE_A;
import static com.kenfogel.isp.constants.ISPConstants.PLANRATE_B;
import static com.kenfogel.isp.constants.ISPConstants.PLANRATE_C;
import static com.kenfogel.isp.constants.ISPConstants.PLANS;
import com.kenfogel.isp.data.ISPFXBean;
import com.kenfogel.isp.data.ISPFXCalculatedBean;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Business class Encapsulates all logic in the program, decoupled from other
 * layers
 *
 * Added method to ISPFXCalculatedBean to clear its values at the start of each
 * calculation
 *
 * @author Ken Fogel
 * @version 1.1
 *
 */
public class ISPBusinessImpl implements ISPBusiness {

    private final ISPFXBean ispBean;
    private final ISPFXCalculatedBean ispCalcBean;

    private final ObservableList<String> plans;

    private final static Logger LOG = LoggerFactory.getLogger(ISPBusinessImpl.class);

    /**
     * Non-default constructor
     *
     * @param ispBean
     * @param ispCalcBean
     */
    public ISPBusinessImpl(ISPFXBean ispBean, ISPFXCalculatedBean ispCalcBean) {
        super();
        this.ispCalcBean = ispCalcBean;
        this.ispBean = ispBean;
        plans = PLANS;
    }

    /**
     * Returns the list of plans from the constant list
     *
     * @return
     */
    @Override
    public ObservableList<String> getPlans() {
        return plans;
    }

    @Override
    public void calculate() {
        LOG.info("start");
        ispCalcBean.clearBean();
        updateISPFXBean();
        calculateExtraHours();
        calculateExtraCost();
        calculateTotalCost();
        LOG.info("end");
    }

    /**
     * Based on the plan chosen this sets the appropriate values in the ISPBean
     */
    @Override
    public void updateISPFXBean() {

        LOG.info("start");

        ispBean.setPlanType(ispCalcBean.getPlanType());
        switch (ispBean.getPlanType()) {
            case "A":
                ispBean.setPlanCost(PLANRATE_A);
                ispBean.setHourlyCost(ADDCOST_A);
                ispBean.setIncludedHours(INCLUDEDHOURS_A);
                break;
            case "B":
                ispBean.setPlanCost(PLANRATE_B);
                ispBean.setHourlyCost(ADDCOST_B);
                ispBean.setIncludedHours(INCLUDEDHOURS_B);

                break;
            case "C":
                ispBean.setPlanCost(PLANRATE_C);
                ispBean.setHourlyCost(ADDCOST_C);
                ispBean.setIncludedHours(INCLUDEDHOURS_C);
                break;
            default:
                break;
        }
        LOG.info("end");
    }

    /**
     * Calculates the number of hours to be charged for, except for plan C and
     * returns the value
     *
     * @return
     */
    private void calculateExtraHours() {
        LOG.info("start");

        if (!ispBean.getPlanType().equals("C") && ispBean.getTotalHours() > ispBean.getIncludedHours()) {
            ispCalcBean.setExtraHours(ispBean.getTotalHours() - ispBean.getIncludedHours());
        }
        LOG.info("end");
    }

    /**
     * Calculates the charge for the additional hours
     *
     * @return
     */
    private void calculateExtraCost() {
        LOG.info("start");

        if (ispCalcBean.getExtraHours() > 0.0) {
            ispCalcBean.setExtraCost(ispCalcBean.getExtraHours() * ispBean.getHourlyCost());
        }
        LOG.info("end");
    }

    /**
     * Calculates the total charge
     */
    private void calculateTotalCost() {
        LOG.info("start");
        ispCalcBean.setTotalCost(ispCalcBean.getExtraCost() + ispBean.getPlanCost());
        LOG.info("end");
    }
}
