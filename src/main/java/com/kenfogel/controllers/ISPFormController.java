package com.kenfogel.controllers;

import com.kenfogel.controls.DoubleTextField;
import com.kenfogel.isp.business.ISPBusinessImpl;
import com.kenfogel.isp.business.ISPBusiness;
import com.kenfogel.isp.data.ISPFXBean;
import com.kenfogel.isp.data.ISPFXCalculatedBean;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class
 *
 * @author Ken
 */
public class ISPFormController implements Initializable {

    private final ISPFXBean bean;
    private final ISPFXCalculatedBean ispCalcBean;
    private final ISPBusiness ispBusiness;

    private final static Logger LOG = LoggerFactory.getLogger(ISPFormController.class);

    @FXML // fx:id="planCombo"
    private ComboBox<String> planCombo; // Value injected by FXMLLoader

    @FXML // fx:id="planHours"
    private TextField planHours; // Value injected by FXMLLoader

    @FXML // fx:id="submitButton"
    private Button submitButton; // Value injected by FXMLLoader

    @FXML // fx:id="planCost"
    private TextField planCost; // Value injected by FXMLLoader

    @FXML // fx:id="includedHours"
    private TextField includedHours; // Value injected by FXMLLoader

    @FXML // fx:id="extraHours"
    private TextField extraHours; // Value injected by FXMLLoader

    @FXML // fx:id="extraCost"
    private TextField extraCost; // Value injected by FXMLLoader

    @FXML // fx:id="totalCost"
    private TextField totalCost; // Value injected by FXMLLoader

    public ISPFormController() {
        super();
        bean = new ISPFXBean();
        ispCalcBean = new ISPFXCalculatedBean();
        ispBusiness = new ISPBusinessImpl(bean, ispCalcBean);
    }

    @FXML
    void updateResult(ActionEvent event) {
        LOG.info("bean.getTotalHours(): " + bean.getTotalHours());
        if (bean.getTotalHours() < 0 || bean.getTotalHours() > 744) {
            // Modal dialog box
            Alert dialog = new Alert(Alert.AlertType.WARNING);
            dialog.setTitle("Invalid Hours");
            dialog.setHeaderText("Hours Less Than Zero or Greater Than 744");
            dialog.setContentText("Please enter hours between 0 and 744.");
            dialog.showAndWait();
            bean.setTotalHours(0);
        } else {

            ispBusiness.calculate();
        }
    }

    /**
     * Initializes the controller class. The Bindings create an automatic update
     * of the fields on the form if the bound value changes.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        planCombo.setItems(ispBusiness.getPlans());

        Bindings.bindBidirectional(planCombo.valueProperty(),
                ispCalcBean.planTypeProperty());
        Bindings.bindBidirectional(planHours.textProperty(),
                bean.totalHoursProperty(),
                new NumberStringConverter());
        Bindings.bindBidirectional(planCost.textProperty(),
                bean.planCostProperty(),
                new NumberStringConverter());
        planCost.setEditable(false);
        Bindings.bindBidirectional(includedHours.textProperty(),
                bean.includedHoursProperty(),
                new NumberStringConverter());
        includedHours.setEditable(false);
        Bindings.bindBidirectional(extraHours.textProperty(),
                ispCalcBean.extraHoursProperty(),
                new NumberStringConverter());
        extraHours.setEditable(false);
        Bindings.bindBidirectional(extraCost.textProperty(),
                ispCalcBean.extraCostProperty(),
                new NumberStringConverter());
        extraCost.setEditable(false);
        Bindings.bindBidirectional(totalCost.textProperty(),
                ispCalcBean.totalCostProperty(),
                new NumberStringConverter());
        totalCost.setEditable(false);

        // Handle TextField text changes.
//        planHours.textProperty().addListener((observable, oldValue, newValue) -> {
//            updateResult(null);
//        });
        // Handle TextField enter key event.
//        planHours.setOnAction((event) -> {
//            updateResult(null);
//        });
    }
}
